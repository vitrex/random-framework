<div class = "container">
	<div class="card">
	  <div class="card-block">
	    <p class="card-text">Register to <strong>dafed social newtork</strong> for free and explore world of people who are passionate with programming.</p>

	    <hr />
	    <h4>Account information</h4>
	    <small class="form-text text-muted">We are not gonna spam your inbox, no worries.</small>
	    <hr />

	    <div class = "row">
	    	<div class = "col-md-12">
	    		<div class="form-group">
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
				 </div>
	    	</div>
	    </div>

	    <hr />
    	<h4>Birth date and location</h4>
    	<small class="form-text text-muted">Since <strong>dafed</strong> is international group it's important.</small>
    	<hr />

	    <div class = "row">
	    	<div class = "col-md-6">
	    		<div class = "row">
	    			<div class = "col-md-6">
	    				<div class = "form-group">
	    					<select class="form-control" id="exampleSelect2">
						     <?php 
						    	foreach($this->serviceProvider->service->getYears() as $year)
						    	{
						    		echo "<option value = '".$year['year']."'>".$year['year']."</option>";
						    	}
						     ?>
						    </select>
	    				</div>
	    			</div>
	    			<div class = "col-md-3">
	    				<div class = "form-group">
	    					<select class="form-control" id="exampleSelect2">
						      <option>January</option>
						      <option>February</option>
						      <option>March</option>
						      <option>April</option>
						      <option>May</option>
						      <option>June</option>
						      <option>July</option>
						      <option>August</option>
						      <option>September</option>
						      <option>October</option>
						      <option>November</option>
						      <option>December</option>
						    </select>
	    				</div>
	    			</div>
	    			<div class = "col-md-3">
	    				<div class = "form-group">
	    					<select class="form-control" id="exampleSelect2">
						      <option>1</option>
						      <option>2</option>
						      <option>3</option>
						      <option>4</option>
						      <option>5</option>
						      <option>6</option>
						      <option>7</option>
						      <option>8</option>
						      <option>9</option>
						      <option>10</option>
						      <option>11</option>
						      <option>12</option>
						      <option>13</option>
						      <option>14</option>
						      <option>15</option>
						      <option>16</option>
						      <option>17</option>
						      <option>18</option>
						      <option>19</option>
						      <option>20</option>
						      <option>21</option>
						      <option>22</option>
						      <option>23</option>
						      <option>24</option>
						      <option>25</option>
						      <option>26</option>
						      <option>27</option>
						      <option>28</option>
						      <option>29</option>
						      <option>30</option>
						      <option>31</option>
						    </select>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div class = "col-md-6">
	    		<div class = "form-group">
					<select class="form-control" id="exampleSelect2">
				      <option>Select Country</option>
				      <option>2</option>
				      <option>3</option>
				      <option>4</option>
				      <option>5</option>
				    </select>
				</div>
	    	</div>
	    </div>

	    <hr />
	    <h4>Personal information</h4>
	    <small class="form-text text-muted">None of personal data will be shared to 3rd parties.</small>
	    <hr />

	    <div class = "row">
	    	<div class = "col-md-4">
	    		 <div class="form-group">
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name">
				 </div>
	    	</div>
	    	<div class = "col-md-4">
	    		 <div class="form-group">
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Surname">
				 </div>
	    	</div>
	    	<div class = "col-md-4">
	    		 <div class="form-group">
				    <select class="form-control" id="exampleSelect2">
				      <option>Sex</option>
				      <option>Male</option>
				      <option>Female</option>
				      <option>Other</option>
				    </select>
				 </div>
	    	</div>
	    </div>

	    <hr />
	    <h4>Interrests</h4>
	    <small class="form-text text-muted">Tell us more about yourself.</small>
	    <hr />

	    <div class = "row">
	    	<div class = "col-md-12">
		 		<div class="form-group">
				    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
				</div>
			</div>
	    </div>
	    <hr />

	    <h4>Quiz</h4>
	    <small class="form-text text-muted">In order to register you must answer following questions prepared by founders of <strong>dafed</strong>.</small>
	    <hr />

	    <div class = "row">
	 		<div class = "col-md-8">
	 			<p>1. Doge is god ?</p>
	 		</div>
	 		<div class = "col-md-2">
	 			<div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1">
			        True
			      </label>
			    </div>
	 		</div>
	 		<div class = "col-md-2">
	 			<div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1">
			        False
			      </label>
			    </div>
	 		</div>
	    </div>

	    <hr />

	    <div class = "row">
	 		<div class = "col-md-8">
	 			<p>2. Another random question ?</p>
	 		</div>
	 		<div class = "col-md-2">
	 			<div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="optionsRadios2" id="optionsRadios1" value="option1">
			        True
			      </label>
			    </div>
	 		</div>
	 		<div class = "col-md-2">
	 			<div class="form-check">
			      <label class="form-check-label">
			        <input type="radio" class="form-check-input" name="optionsRadios2" id="optionsRadios1" value="option1">
			        False
			      </label>
			    </div>
	 		</div>
	    </div>

	    <hr />

	    <button class = "btn btn-sm btn-block btn-success">Apply</button>

	    <hr />
	    <small class="form-text text-muted">All candidates will recieve email withing 24 hours after registration form applied. And only those who are aproved by <strong>dafed</strong> team gonna recieve confirmation email with instructions how access the social network platform.</small>
	  </div>
	</div>
</div>
