<?php 
// Register ServiceProvider
require_once 'app/Providers/RouteServiceProvider.php';

class index 
{
	public $config;
	public $router;

	public function __construct()
	{

		// create router
		$this->router = new RouteServiceProvider();

		// Call for pages so thei inherit all the controllers and stuff
	}

	public function constructPage($url)
	{
		$this->router->map($url);
	}
}

// lets auto load here i guess
$url = isset($_GET['p']) ? $_GET['p'] : "index";
$page = new index();
$page->constructPage($url);