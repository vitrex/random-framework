<?php 
// Register ContentController
require_once 'app/Providers/ServiceProvider.php';

class ContentController
{
	public $serviceProvider;
	public $config;

	function __construct()
	{
		// Include and register app.json
		$appJson = file_get_contents('app/app.json');
		$this->config = json_decode($appJson);
		
		$this->serviceProvider = new ServiceProvider();
	}

	function render($view)
	{
		// load services
		$this->serviceProvider->loadService($view);

		// render page
		include 'views/includes/header.php';
			include 'views/pages/'.$view.".php";
		include 'views/includes/footer.php';
	}
}