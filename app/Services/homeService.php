<?php 

class homeService 
{
	function __construct()
	{
		
	}
	
	function getyears()
	{
		$currentYear = date("Y");
		$minYear = $currentYear - 100;
		$years = [];
		while($minYear <= $currentYear)
		{
			$years[] = ['year' => $minYear++];
		}
		
		return $years;
	}
}