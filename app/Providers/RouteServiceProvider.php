<?php 
// Register ContentController
require_once 'app/Controllers/ContentController.php';

class RouteServiceProvider 
{	
	public $routes;
	public $contentController;

	public function __construct()
	{
		$this->routes = [];

		$this->routes = [
			'index' => [ 'uri' => 'home'],
			'login' => [ 'uri' => 'login'],
		];

		// register ContentController
		$this->contentController = new ContentController();
	}

	public function map($url)
	{

		// construct all the shit
		if(file_exists("views/pages/".$this->routes[$url]['uri'].".php"))
		{
			$this->contentController->render($this->routes[$url]['uri']);
		}
		else 
		{
			$this->contentController->render("404");
		}

	}
}