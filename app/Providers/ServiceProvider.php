<?php 

class ServiceProvider 
{
	public $service;
	
	function __construct()
	{
		
	}

	function loadService($data)
	{
		if(file_exists('app/Services/'.$data.'Service.php'))
		{
			include 'app/Services/'.$data.'Service.php';
			$className = $data."Service";
			$this->service =  new $className();
			
			return $this->service;
		}
	}
}